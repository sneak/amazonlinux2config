export GOPATH="/usr/local/go"
if [[ ! -d $GOPATH ]]; then
    sudo mkdir -p $GOPATH
    chown $(whoami):$(whoami) $GOPATH
fi

export PATH="$PATH:$HOME/bin"
export PATH="$PATH:$HOME/.yarn/bin"
_byobu_sourced=1 . /usr/bin/byobu-launch
