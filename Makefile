default: install

install: prereqs
	cat ./profile.sh > $(HOME)/.profile
	bash setup.sh

prereqs:
	sudo yum install -y rsync byobu
