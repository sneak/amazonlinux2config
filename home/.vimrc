" this has to be first apparently?
" i had problems when it was lower in the file.
let mapleader=","

let g:go_version_warning = 0

silent !mkdir -p ~/.vim/plug > /dev/null 2>&1
silent !mkdir -p ~/.vim/backup > /dev/null 2>&1
silent !mkdir -p ~/.vim/undo > /dev/null 2>&1
silent !mkdir -p ~/.vim/swap > /dev/null 2>&1
silent !mkdir -p ~/.vim/autoload > /dev/null 2>&1
silent !mkdir -p ~/.vim/plugged > /dev/null 2>&1


"let g:go_version_warning = 0

if empty(glob('~/.vim/plug/plug.vim'))
  silent !curl -fLo ~/.vim/plug/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

source ~/.vim/plug/plug.vim

call plug#begin('~/.vim/plug')

Plug 'nathanaelkane/vim-indent-guides'
let g:indent_guides_enable_on_vim_startup = 1

Plug 'tpope/vim-sensible'
Plug 'preservim/nerdtree'
Plug 'ryanoasis/vim-devicons'
"Plug 'vim-airline/vim-airline'
"Plug 'vim-airline/vim-airline-themes'
Plug 'fatih/vim-go', { 'do': ':GoInstallBinaries' }
Plug 'prettier/vim-prettier', {
  \ 'do': 'yarn install',
  \ 'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'json', 'graphql', 'markdown', 'vue', 'yaml', 'html'] }

let g:prettier#autoformat = 1
let g:prettier#autoformat_require_pragma = 0


Plug 'fatih/molokai'
"Plug 'morhetz/gruvbox'
Plug 'sbdchd/neoformat'
"Plug 'LnL7/vim-nix'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" syntastic
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Plug 'vim-syntastic/syntastic'


set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_go_checkers = ['go']

call plug#end()

let g:rehash256 = 1
let g:molokai_original = 1
let g:gruvbox_italic = 1
"colorscheme molokai
"colorscheme gruvbox

noremap <Leader>w :w<cr>
noremap <Leader><cr> i<cr><Esc>
noremap <Leader>l :GoMetaLinter<cr>
noremap <Leader>b :GoBuild<cr>
noremap <Leader>f :Neoformat<cr>
noremap <Leader>t :NERDTreeToggle<CR>


" vim-go golang configuration:
let g:go_fmt_fail_silently = 0
let g:go_fmt_command = "goimports"
let g:go_addtags_transform = "camelcase"
let g:go_highlight_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_function_calls = 1
let g:go_highlight_extra_types = 1

let g:go_def_mode='gopls'
let g:go_info_mode='gopls'
let g:go_auto_type_info = 1


highlight SyntasticErrorSign guifg=white guibg=red

"Plug 'scrooloose/nerdtree'
"Plug 'ludovicchabant/vim-gutentags'

let g:gutentags_ctags_tagfile = '.tags'
let g:gutentags_file_list_command = 'git ls-files'
let g:gutentags_generate_on_new = 1

"Plug 'majutsushi/tagbar'
"Plug 'fatih/vim-go', { 'do': 'GoUpdateBinaries', 'for': 'go' }
"Plug 'cespare/vim-toml', { 'for': 'toml' }
"call plug#end()


"let g:airline_powerline_fonts = 1

"if !exists('g:airline_symbols')
"    let g:airline_symbols = {}
"endif

" unicode symbols
" let g:airline_left_sep = '▶'
" let g:airline_right_sep = '◀'
" let g:airline_symbols.crypt = '🔒'
" let g:airline_symbols.linenr = '☰'
" let g:airline_symbols.linenr = '␊'
" let g:airline_symbols.linenr = '␤'
" let g:airline_symbols.linenr = '¶'
" let g:airline_symbols.maxlinenr = ''
" let g:airline_symbols.maxlinenr = '㏑'
" let g:airline_symbols.branch = '⎇'
" let g:airline_symbols.paste = 'ρ'
" let g:airline_symbols.paste = 'Þ'
" let g:airline_symbols.paste = '∥'
" let g:airline_symbols.spell = 'Ꞩ'
" let g:airline_symbols.notexists = 'Ɇ'
" let g:airline_symbols.whitespace = 'Ξ'

fun! InitBex()
    let myvar = strftime("%y%m%d.%Hh%M")
    let myvar = "set backupext=.". myvar
    execute myvar
endfun
map <silent> <Esc>:call InitBex()<CR>
silent call InitBex()

set relativenumber
filetype plugin indent on
syntax on

set modelines=10
"noremap  <Up> ""
"noremap! <Up> <Esc>
"noremap  <Down> ""
"noremap! <Down> <Esc>
"noremap  <Left> ""
"noremap! <Left> <Esc>
"noremap  <Right> ""
"noremap! <Right> <Esc>
"vnoremap > ><CR>gv 
"vnoremap < <<CR>gv 

"au BufWinLeave * silent! mkview
"au BufWinEnter * silent! loadview

au BufRead,BufNewFile *.go set filetype=go
"au BufRead,BufNewFile *.coffee set filetype=coffee


set ignorecase
set smartcase
set gdefault
set incsearch
set showmatch
set hlsearch
"nnoremap <leader><space> :noh<cr>

set bg=dark
set ignorecase
set smartcase
set gdefault
set incsearch
set showmatch
set hlsearch
set autoindent
"set signcolumn=yes
set backspace=indent,eol,start
set backup
set backupdir=~/.vim/backup//
set cmdheight=3
"set clipboard=unnamed
set directory=~/.vim/swap//
set encoding=utf-8
set expandtab
set fileencodings=utf-8
set guifont=Monaco:h16
set hidden
set ignorecase
set laststatus=2
set modeline
set nobomb
set nocompatible
set shiftwidth=4
set showcmd
set smartcase
set smarttab
set softtabstop=4
set spellfile=~/.vim/spellfile.utf-8.add
set spelllang=en
set tabstop=4
set termencoding=utf-8
set ttyfast
set tw=76
set undodir=~/.vim/undo//
set undofile
set wildignore+=*.pyc
set writebackup

setglobal fileencoding=utf-8

autocmd FileType make setlocal noexpandtab
autocmd FileType markdown setlocal spell
au BufNewFile,BufFilePre,BufRead *.md set filetype=markdown
autocmd BufNewFile,BufRead *.json set ft=javascript

autocmd BufWritePre *.js Neoformat
autocmd BufWritePre *.jsx Neoformat
autocmd BufWritePre *.json Neoformat
autocmd BufWritePre *.css Neoformat

"inoremap jj <Esc>

"map N Nzz
"map n nzz
"map <s-tab> <c-w><c-w>
"colorscheme koehler
hi CursorLine term=none cterm=none ctermbg=none
set t_Co=256
set foldmethod=indent
set foldminlines=5
set nofoldenable        "dont fold by default
set foldlevel=1         "this is just what i use

"nnoremap <silent> <Space> @=(foldlevel('.')?'za':'l')<CR>
"vnoremap <Space> zf

nnoremap <silent> <Tab> :bn<CR>
nnoremap <silent> <Backspace> :bp<CR>
nnoremap <silent> <Leader><Enter> :ls<CR>

highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

let python_no_builtin_highlight = 1
let python_no_doctest_code_highlight = 1
let python_no_doctest_highlight = 1
let python_no_exception_highlight = 1
let python_no_number_highlight = 1
let python_space_error_highlight = 1

let g:flake8_show_in_gutter=1  " show
