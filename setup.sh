#!/bin/bash
source ./profile.sh

PKGS="
    byobu
    go
    vim
"
sudo amazon-linux-extras install epel -y
sudo amazon-linux-extras install docker -y
sudo yum -y install $PKGS
sudo yum upgrade -y


export GOPATH=/usr/local/go
sudo chown -Rv $(whoami):$(whoami) $GOPATH
rm -rf $HOME/go

rsync -avP ./home/ $HOME/

chmod 400 $HOME/.ssh/*

sudo npm install -g yarn

yarn global add prettier

# setup vim
echo "" | vim +PlugInstall +qall
vim +GoInstallBinaries +qall
vim +PlugInstall +qall

# configure byobu
byobu-launcher-install
